from werkzeug.wrappers import cached_property
from functools import lru_cache
import random

class Test:
    @cached_property
    def test1(self):
        """cached_property"""
        return random.randint(0,999999999999)

    @lru_cache(maxsize=None)
    def _cached(self):
        return random.randint(0,999999999999)
    
    @property
    def test2(self):
        """property + lru_cache(maxsize=None)"""
        return self._cached()

if __name__ == '__main__':
    import timeit
    t0 = timeit.timeit('t.test1', setup='from test import Test; t=Test()')
    t1 = timeit.timeit('t.test2', setup='from test import Test; t=Test()')
    faster = min(t0, t1)
    t0p = int((t0/faster)*100) - 100
    t1p = int((t1/faster)*100) - 100
    
    for tdoc, t, tp in (
        (Test.test1.__doc__, t0, t0p), 
        (Test.test2.__doc__, t1, t1p)):
        
        if tp:
            print("{}: {} ({}% slower)".format(tdoc, t, tp))
        else:
            print("{}: {}".format(tdoc, t))
