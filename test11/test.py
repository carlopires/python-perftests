import uuid
from uuid import UUID

def test1(v):
    """v.__class__ is uuid.UUID"""
    return v.__class__ is uuid.UUID

def test2(v):
    """v.__class__ is UUID"""
    return v.__class__ is UUID

if __name__ == '__main__':
    import timeit
    t0 = timeit.timeit('test1(v0)', setup='from test import test1, uuid; v0 = uuid.uuid4()')
    t1 = timeit.timeit('test2(v0)', setup='from test import test2, uuid; v0 = uuid.uuid4()')
    faster = min(t0, t1)
    t0p = int((t0/faster)*100) - 100
    t1p = int((t1/faster)*100) - 100
    
    for tdoc, t, tp in (
        (test1.__doc__, t0, t0p), 
        (test2.__doc__, t1, t1p)):
        
        if tp:
            print("{}: {} ({}% slower)".format(tdoc, t, tp))
        else:
            print("{}: {}".format(tdoc, t))
