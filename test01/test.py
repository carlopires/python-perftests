class A:
    pass

a = A()

def test1():
    return a.__class__.__name__

def test2():
    return type(a).__name__

if __name__ == '__main__':
    import timeit
    print('a.__class__.__name__', timeit.timeit('test1()', setup='from test import test1'))
    print('type(a).__name__', timeit.timeit('test2()', setup='from test import test2'))
