def test1(v):
    """any((None, None))"""
    return any(v)

def test2(v):
    """v[0] is not None"""
    return True if v[0] else False

if __name__ == '__main__':
    import timeit
    t0 = timeit.timeit('test1((None, None))', setup='from test import test1; d={}')
    t1 = timeit.timeit('test2((None, None))', setup='from test import test2; d={}')
    faster = min(t0, t1)
    t0p = int((t0/faster)*100) - 100
    t1p = int((t1/faster)*100) - 100
    
    for tdoc, t, tp in (
        (test1.__doc__, t0, t0p), 
        (test2.__doc__, t1, t1p)):
        
        if tp:
            print("{}: {} ({}% slower)".format(tdoc, t, tp))
        else:
            print("{}: {}".format(tdoc, t))
