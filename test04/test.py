def test1():
    """''.__class__ is str"""
    return ''.__class__ is str

def test2():
    """isinstance('', str)"""
    return isinstance('', str)

if __name__ == '__main__':
    import timeit
    t0 = timeit.timeit('test1()', setup='from test import test1')
    t1 = timeit.timeit('test2()', setup='from test import test2')
    faster = min(t0, t1)
    t0p = int((t0/faster)*100) - 100
    t1p = int((t1/faster)*100) - 100
    
    for tdoc, t, tp in (
        (test1.__doc__, t0, t0p), 
        (test2.__doc__, t1, t1p)):
        
        if tp:
            print("{}: {} ({}% slower)".format(tdoc, t, tp))
        else:
            print("{}: {}".format(tdoc, t))
