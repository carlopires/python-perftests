class A:
    pass

a = A()

def test1():
    return a.__class__

def test2():
    return type(a)

if __name__ == '__main__':
    import timeit
    print('a.__class__', timeit.timeit('test1()', setup='from test import test1'))
    print('type(a)', timeit.timeit('test2()', setup='from test import test2'))
